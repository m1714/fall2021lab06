package fall2021lab06;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * @author Nolan Ganz
 */
public class RpsChoice implements EventHandler<ActionEvent> {

    private TextField messageField;
    private TextField winsField;
    private TextField lossesField;
    private TextField tiesField;
    private String playerChoice;
    private RpsGame rps;

    public RpsChoice(TextField messageField, TextField winsField, TextField lossesField, TextField tiesField,
            String playerChoice, RpsGame rps) {
        this.messageField = messageField;
        this.winsField = winsField;
        this.lossesField = lossesField;
        this.tiesField = tiesField;
        this.playerChoice = playerChoice;
        this.rps = rps;
    }

    @Override
    public void handle(ActionEvent e) {
        this.messageField.setText(this.rps.playRound(this.playerChoice));
        this.winsField.setText("Wins: "+this.rps.getWins());
        this.lossesField.setText("Losses: "+this.rps.getLosses());
        this.tiesField.setText("Ties: "+this.rps.getTies());
    }
}
