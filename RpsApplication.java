package fall2021lab06;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * @author Nolan Ganz
 */
public class RpsApplication extends Application {

    private RpsGame rps = new RpsGame();

    @Override
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

        HBox buttons = new HBox();
        HBox textFields = new HBox();
        VBox overall = new VBox();

        TextField messageField = new TextField("Welcome!");
        messageField.setPrefWidth(200);
        TextField winsField = new TextField("Wins: 0");
        TextField lossesField = new TextField("Losses: 0");
        TextField tiesField = new TextField("Ties: 0");

        Button rockButton = new Button("rock");
        rockButton.setOnAction(new RpsChoice(messageField, winsField, lossesField, tiesField, "rock", rps));
        Button paperButton = new Button("paper");
        paperButton.setOnAction(new RpsChoice(messageField, winsField, lossesField, tiesField, "paper", rps));
        Button scissorsButton = new Button("scissors");
        scissorsButton.setOnAction(new RpsChoice(messageField, winsField, lossesField, tiesField, "scissors", rps));

        buttons.getChildren().addAll(rockButton,paperButton,scissorsButton);
        textFields.getChildren().addAll(messageField,winsField,lossesField,tiesField);
        overall.getChildren().addAll(buttons,textFields);
        root.getChildren().add(overall);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}
